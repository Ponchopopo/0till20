﻿#include <iostream>
#include <clocale>
using namespace std;

int const N = 10;

void PrintOddEven(bool IsEven, int N)
{
    int Count;
    if (IsEven)
    {
        cout << "Четные числа от 0 до " << N << ": ";
        Count = 0;
    }
    else
    {
        cout << "Нечетные числа от 0 до " << N << ": ";
        Count = 1;
    }

    while (Count <= N)
    {
        cout << Count << " ";
        Count += 2;
    }

    cout << "\n";
}

int main()
{
    setlocale(LC_CTYPE, "rus");

    cout << "Четные числа от 0 до " << ::N << ": ";
    for (int i = 0; i <= N; i += 2)
    {
        cout << i << " ";
    }
      cout << "\n";

    PrintOddEven(true, 20);
    PrintOddEven(false, 20);
}